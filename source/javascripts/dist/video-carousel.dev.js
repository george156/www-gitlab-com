"use strict";

$('.video-slider').slick({
  dots: false,
  centerMode: false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: true,
  nextArrow: '<div class="slick-arrow-next"><i class="fas fa-chevron-right"></i></div>',
  prevArrow: '<div class="slick-arrow-prev"><i class="fas fa-chevron-left"></i></div>',
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3
    }
  }, {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }, {
    breakpoint: 480,
    settings: {
      slidesToShow: 1.25,
      slidesToScroll: 1.5,
      arrows: false,
      centerMode: true,
      infinite: false,
      centerPadding: 0
    }
  }]
});
$('#videoCarouselModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var videoUrl = button.data('video-link');
  var modal = $(this);
  modal.find('.modal-title').text(videoUrl);
  modal.find('.share-text').text(videoUrl);
  modal.find('.carousel-modal-iframe').attr("src", videoUrl);
});
$('.btn-close-modal').click(function (e) {
  e.preventDefault();
  $('.carousel-modal-iframe').attr('src', '');
});