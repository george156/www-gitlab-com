---
layout: handbook-page-toc
title: "Stock Compensation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


**Applications Used**

| Application | Primary Use |
| ------ | ------ |
|Carta| Stock option administration, capitalization table and equity management software platform. |
| Netsuite | ERP system used by the Finance team to record the financial activity |
| BambooHR | For eligible team members details |

**Introduction**

* GitLab's equity award program is described on our [Stock Options page in the handbook.](/handbook/stock-options/)


# 1. Option Administration 

[Link to flowchart](https://drive.google.com/open?id=1ltSw0RIU_aTunUqjhrj9R_oGmcQx0541yYs3f3p2YU0)

The Stock Options Administrator is responsible for getting the relevant new hire equity information to the database and retrieving exercise information for GitLab
Carta is the equity administration platform used by the Stock Options Administrator to manage the equity information of GitLab team members. 

* [Carta admins can have 3 types of clearances:](https://drive.google.com/open?id=1oi_7QUs7te3OwEmB1bD9cEKxQaJV9xXqGQ5FP4Akdis)

   1. Legal Administrator - Legal administrators can view and edit all pages under the account. They can invite all users to the company.  (Ex: Fenwick Legal Team, CFO, Stock Options Administrator)

   1. Company Editor - Company Editors can view all pages, edit pages under the company section and invite company editors and viewers. Editors can also draft securities but can NOT issue, modify or transfer securities. (Senior Manager, Accounting and External Reporting)

   1. Company Viewer - Company Viewers can view all pages under the account, except for Settings and Access under Company and Expense Accounting under Compliance. Viewers can also download all reports. (Ex: Compensation and Benefits Manager)

* You cannot add users that are higher than your permission set. Users can only be removed by someone with the same or higher permissions.

* To add a new admin:
  * Click on the [company tab](https://docs.google.com/document/d/1iCrg_EP3Mw4zORF_qdEk3IQmwocS0K5NNAt35cvYsiI/edit?usp=sharing)
  * Click on the [add user button](https://docs.google.com/document/d/1yUr_IcOMgSMz0S5bm6AIAId6UopxG_rmMWMlnIo0vEg/edit?usp=sharing)
  * Fill in the necessary [details](https://docs.google.com/document/d/1qLZ7O2cFrAXYWeKpNOlWlB9i5w8_E6ZgjCVqOt_J6bM/edit?usp=sharing) like 
    * First and last name
    * Email
    * Title
    * Permissions<br><br>
* To change the access rights of an admin, click on [edit](https://docs.google.com/document/d/1juIvQyasNQddjCWmrLiQyUNT0ZVsH_EsSiv8MCJ72Gw/edit?usp=sharing)
* Change the appropriate [fields](https://docs.google.com/document/d/1SUY2psIm81flajNwrCgeRwwM2IX9E7ZAwDNXCx697MQ/edit?usp=sharing)
* Information on adding/deleting an admin comes from the CFO via an email
* When team members have questions that need to be answered, they can pose a question on [Carta](https://docs.google.com/document/d/1lNN0MFkios25pz_7bQpWcDs4D9RsGdtjstP0zYBAjsE/edit?usp=sharing). The system is set up that the CFO receives email notification whenever an team member poses a question. 
    * To pose a question:
      * Navigate to [holdings](https://docs.google.com/document/d/1c6mta-s8q3zsztF9-Roj7sS8IPAIOoL3CoQFGlE7t3Q/edit?usp=sharing)
      * Click on your [option agreement](https://docs.google.com/document/d/1cHDiSLFWtdmutiQCvVcexoe8mcQZ4ChhRRNWW9ZhYtA/edit?usp=sharing)
      * Go to [report issue](https://docs.google.com/document/d/1t_cZxXEde6K6koksfb5pDB7aBb6YVGOCmLKajdbFvVg/edit?usp=sharing)<br><br>
* Company Financials are uploaded by the CFO to help the team members understand the performance of GitLab. 
   * To view company financials, click on the [company tab](https://docs.google.com/document/d/1rqMpDVUPQqrRkMYMcKWaGCCWHiqsyHu8D3U3jEq3HoU/edit?usp=sharing)
   * Click on [Financials](https://docs.google.com/document/d/1ZUJvtSMHHemxZKJisStXeZQOoGD5rTjOpk2jSLZn3_w/edit?usp=sharing)
   * The library contains various legal documents like incorporation documents, securities filings, closing volumes, minute books, disclosures, etc. To go to the library, click on the library option in the [company tab](https://docs.google.com/document/d/1iGefENkKj2tYUu0dRR6h6MvXBNm20MmgpvcfyhwaFMs/edit?usp=sharing)<br><br>
 * When an offer needs to be made to an team member, all the eligible team member names (eligible = not approved by the Board) are pulled from Bamboo HR via a report **(ESOP.C.01)** . To access this report:
   * Log in to [Bamboo HR](https://docs.google.com/document/d/1OgzdR_kcbC1TFZTD6bAdy3-s1MtyJ2BVR2S6xYLC_bY/edit?usp=sharing)
   * Click on [Reports](https://docs.google.com/document/d/1nuvJizxsYEhPmahDgVs3DHgMK4VNIrwyw_O5jH-cTNA/edit?usp=sharing)
   * Click on the [Stock Options Report](https://docs.google.com/document/d/1lH0FrmKTtRX7Dqe06LZHFvBLTe9ipGvC7SmP4NApaUk/edit?usp=sharing)
   * Export the [Report](https://docs.google.com/document/d/15DBLnCAUt0GJnljDBM6bGzwAq4oumdX6OJ0lJ0RcQto/edit?usp=sharing) as an Excel file
   * An [spreadsheet](https://docs.google.com/document/d/1wyNFgRQWOCeUuRdNGon5GzwZ0n8eMpgh24UeK7Olfi8/edit?usp=sharing) is generated, and a Google Doc is created with the list of all the team member names, the type of grant, the number of options, and the vesting schedule. 
   * This is created by the Stock Options Administrator. The CFO approves this Google Doc, and Fenwick prepares an [exhibit](https://docs.google.com/document/d/1EyKAxhu6og2DcIerV7myJZe-Of11I9siCn-_V23YaOA/edit?usp=sharing) to be presented to the Board **(ESOP.C.01)** .  
   * This exhibit contains the Optionee Name, the number of shares awarded, the vesting start date, the type of award, the Country/state of residence, the vesting schedule, and whether the options can be early exercised or not. 
   * The board approves each offer **(ESOP.C.01)**.
   * After the Board approves the exhibit, the Stock Options Administrator can then add this data to Carta **(ESOP.C.02)** .
   * The Stock Options Administrator takes the list and puts together a template for upload onto Carta.
   * To upload equity information onto Carta, click on [securities tab](https://docs.google.com/document/d/1uFMUcCgz5qfpMQ1ub9QTeQ1GRormttIy3d_4i08GOts/edit?usp=sharing), and go to equity awards
   * Here you can see the equity information of each team member, including status, quantity, strike price, type, grant date, and the vesting schedule. Click on the [draft options button](https://docs.google.com/document/d/112DkXop7V-bZl8p185lI3cIQS6gKaOFE9pj8EXsbrt4/edit?usp=sharing).
   * Go to [Add Option Grants](https://docs.google.com/document/d/1atMqJi0nYWQ1TMDTZ6Qoc5KjXwCsKRgGrpPTkfGArHk/edit?usp=sharing) and click on import multiple option grants:
   * Download the [Template](https://docs.google.com/document/d/1yfNKYDQ0mdC0Ljlyp2YD2V5VoZiBs8GL4cNd3UWZcrk/edit?usp=sharing), and enter the new equity information.
   * After entering, upload the template onto Carta
   * After this information is entered, an email is sent to the CFO by the Stock Options Administrator to review the data. Any discrepancies with the data are raised to the Stock Options Administrator. The CFO reviews the data on Carta and approves **(ESOP.C.02)**. CFO compares the data to the exhibit presented to the Board. After the CFO reviews the data, the send button is hit by the CFO, and emails go out to the team members telling them that they’ve been granted options **(ESOP.C.02)** .
   * Final approval for Option granting is the [CFO](https://docs.google.com/document/d/11zuvrahl9oBiqqxyQ04pQCDl8tU3M0r-oH5zF0ssO8Y/edit?usp=sharing).<br><br>
 * After approvals are given on Carta. the team members are added to Carta, and [emails](https://docs.google.com/document/d/1PdZ9EKESYnjKZLFpK1l1IBoqxsZuMGJbQf1fRzMZBCE/edit?usp=sharing) are generated by Carta so that the team member can create an account to accept their share grant and log on to and use carta **(ESOP.C.02)** .
   * To accept a grant that has been offered to you, go to the tasks tab, and click on [accept grants](https://docs.google.com/document/d/1EXX_mrnm10uFgR7tUsDee6F8bpzy7d4SQ3ecKV6QQxw/edit?usp=sharing)
   * The [current screen](https://docs.google.com/document/d/1bEk-arWZ6m_hXNxDXap4X17OOSE6u8DXWGG-NTw9lg0/edit?usp=sharing) shows the Grant ID, the company, the option holder name, the quantity, and the vesting schedule.
   * [Check of the boxes](https://docs.google.com/document/d/11MiV4sp0tnbt6xp-4EgxVf5do3GByKTf2vKspNPcInA/edit?usp=sharing) that show you’ve read the following agreements and sign below on the option agreement to accept 
   * After accepting the grant, the [completion screen](https://docs.google.com/document/d/1sCdYQSpA-HmwdLRHlJTsf1-C8WMxvnFVOmL5wlRebvs/edit?usp=sharing) shows up
   * The following is the [home screen of Carta](https://docs.google.com/document/d/1Rb5_yXeASPuc2Rvmp3OJQwHBpq-iZDBvphix9lS3afk/edit?usp=sharing) for an team member logging in to view their options. 
   * The [Holdings tab](https://docs.google.com/document/d/15vZ8SZcnzWsoxdsJ2iulf-1j_tyEjLXSgptmDNtNRro/edit?usp=sharing) shows you an overview of the options that you hold along with the vesting schedule that they’re on, and what you’ve exercised.
   * Clicking on the [graph](https://docs.google.com/document/d/1qQU3Q6sX1BeUhj5ejvGOLTh7jNmaDxTO9HB79RwB7gU/edit?usp=sharing) at any point shows you the number of shares that have vested at that point of time.
   * To view detailed information on your equity award, click on the [Option ID ](https://docs.google.com/document/d/1m4ycpiAoXeeWZExCb7SWvG2zVZ6gIR2zmLJ_Pl--fUw/edit?usp=sharing)
   * A [Pop Up](https://docs.google.com/document/d/1m4ycpiAoXeeWZExCb7SWvG2zVZ6gIR2zmLJ_Pl--fUw/edit?usp=sharing) comes up and contains detailed info about the grant that was awarded to you.
   * Clicking on the [exercise periods tab](https://docs.google.com/document/d/1lPRumpYP96uHZlH8XkaWkWfUX9RD5a46Oe4P867dw3o/edit?usp=sharing) shows you the time that you have to exercise after you leave the company (in various ways), as well as the expiration date of the option grant.
   * Click on the [vesting schedule tab](https://docs.google.com/document/d/1YE7CXVcWZLMbEQiJeYdSMG3-WautXWN8zsV05LKPvrw/edit?usp=sharing) to see the vesting schedule of your options
   * If someone has more than one set of options with [different vesting schedules](https://docs.google.com/document/d/1lwC5zDaXQwuXUVFglEMy70PCDqv5yFbylvm3F9FMcqs/edit?usp=sharing), this is displayed on Carta
   * Click on [documents and notes](https://docs.google.com/document/d/1VEAIO5ale2BuZ7kL7yrkhnjBRruYpPVgpSSpwHVUOJI/edit?usp=sharing) to see your option agreement, incentive plan, exercise agreement, and disclosure packet:
   * To see the approvals granted to your account, click on the [approvals tab](https://docs.google.com/document/d/1-Ks8ERFj0a4wNNU8v44hKFyD0apSRP7wGa6tmm9aFnM/edit?usp=sharing)<br><br>
* To terminate an existing stakeholder from Carta:
  * Click on the [stakeholders tab](https://docs.google.com/document/d/153aAO7nQEK3L5BDqqr9CINnJnjPvAvConNSc20kKt7g/edit?usp=sharing)
  * Select the team member and click [terminate](https://docs.google.com/document/d/1dskCret3xtljR2vJAdQ7IScx6Xo1V3BFiCXz8srj8jI/edit?usp=sharing).
  * Begin entering termination information
  * Termination Date
  * Termination Type = Voluntary or Involuntary
  * New Relationship = Ex-team member
  * Notifications - Add a new email, which is the personal email address and can be found under their profile<br><br>
* For shares being repurchased: Confirm whether the dates to exercise and repurchase are correct and check “I confirm the last day to repurchase is correct for each certificate” and save the termination
  * To repurchase a certificate, navigate to 'Shares' under Securities. Then select 'Repurchase shares' in the drop-down menu for the appropriate tranche.
  * Enter the certificate repurchase information (date, number of shares, transaction value) and make sure the “Transfer funds via ACH” toggle is on. 
  * Next: review repurchase details. Review the details of the repurchase (shares, value, date) and confirm repurchase
  * The final screen will confirm the total funds that will be withdrawn from the company account and transferred to the team member for the repurchase of the shares.
  * Send email to the team member bcc'ing the CFO and Peopleops. A sample email is below:
    * “Hello,[team member], just to confirm that GitLab has initiated a repurchase of your unvested shares. The transaction has been processed through Carta. You should expect to see an ACH transaction into your bank account in the amount(s) of $xxx.xx (and $x,xxx.xx, if necessary). Please contact me if you have any concerns or questions.”
  * Send to their personal email address
  * File email in GitLab Inc=> Equity => Repurchases
  * Send note to Accounting and External Reporting, and get screenshot of transfer out of Comerica
  * File screen shot as above.
  * Once all steps are completed, ensure the offboarding issue is marked as such by checking the box for Carta, assigned to the Stock Plan Administrator<br><br>

* To exercise a share, there are two methods to exercise your shares:
  * Electronic (US Residents Only)
    * Log into your Carta account
    * Follow the directions to enable ACH payments from your bank
    * After ACH has been enabled select exercise option grants and follow the prompts
  * Manual (Non ACH and Non US Residents)
    * Log into your Carta account
    * On Portfolio Details, click the "Holdings" option
    * On the option grant list, click the arrow down icon on right side of the screen and select the "View" option
    * A modal will open. On the left side menu, click "Documents and notes"
    * Download the "Form of Exercise Agreement" file
    * Complete the form, sign, and return as PDF to stockadmin@gitlab.com
    * Send payment in US dollars by wire transfer. You will be provided the wire transfer info.
* Note for US residents: whichever method you choose, be sure to download the 83-b election form provided by Carta and file with the IRS within 30 days of exercise. Send a copy of the [signed and dated election form](https://docs.google.com/document/d/1PhnDFadnO061MSsj7HFaxbeHsGpKzz7KfZMnqcBFgik/edit?usp=sharing) to stockadmin@gitlab.com. The Stock Plan Administrator will file it within Bamboo/HR.
* To exercise options, go to the holdings tab and click [exercise](https://docs.google.com/document/d/1zSd23vfGHc2WN_akxeVaa06YOqvMHzfgjBt1b13vuwY/edit?usp=sharing).
* Follow the prompts and instructions as shown on the Carta screen.
* The Final approvals for exercising shares are the [CFO and the CLO](https://docs.google.com/document/d/1hXY8fGfC5CIko3RBNH-voJBr3OU4M6qnA1DYzxCQ2Fs/edit?usp=sharing) **(ESOP.C.03)**. 

# 2. Accounting for Stock Options DTP 

[Link to flowchart](https://drive.google.com/open?id=1nY4dPSrriZ6QYZMFhUJLsQJtd3l7QQbg-6tG7sNR8po)

**Introduction**

On a monthly basis, the Senior Manager, Accounting and External Reporting Pulls the stock-based compensation expense report from Carta and uses them to book entries on NetSuite **(ESOP.C.04)**. A high-level fluctuation analysis review is done monthly, and any significant variances are analyzed.
Cash activities related to stock options (buybacks and exercises) are booked during the month. This is done using the options exercised and options canceled reports from Carta. These 2 reports are also compared to reconcile cash and check whether all payments related to buybacks are paid out. In the situations where cash is received but no entry is booked on NetSuite or when there is an entry on NetSuite even though no cash is received, a receivable is booked on NetSuite.

**Downloading Reports from Carta**

To download reports from Carta:
* Login to [Carta](https://docs.google.com/document/d/1RtGG7RbrHrIj-ST4EhROhSw9T2pD2HvcKrHanyTJgMk/edit?usp=sharing)
* Enter your [email ID and password](https://docs.google.com/document/d/1B0gA2W5mFjL-_dcwlnInQ750_1ZkKliNKGJ84sDtbTA/edit?usp=sharing)
* Click on the [compliance tab, and then select Expense Accounting](https://docs.google.com/document/d/1YBsQihjGrZ4Cj9DIstaO1gS9WHnJwN9mskaWgPgcesU/edit?usp=sharing)
* Click Create New Report to [run and download](https://docs.google.com/document/d/1-GNR7RL1eoNeH5vQiLL-EgiI7EJygOPQ-Oqhf7_DhNA/edit?usp=sharing) the Expense Report
* Click [Next](https://docs.google.com/document/d/1gLrKpmnI4MqMhal3_RcCSsBWBVIHN5jGtVgx9GyiqMc/edit?usp=sharing) 1st time
* Click [Next](https://docs.google.com/document/d/1aGCQix6IZnVkovnD0JYIs_GsR5ENofTAqHhndMwK9Ww/edit?usp=sharing) 2nd time
* Click [Next](https://docs.google.com/document/d/1p_CaFMdwCMyORr5EA7ZF3cTj6-T53KQt2FjbcZ6JjRI/edit?usp=sharing) 3rd time
* Select the Reporting Period, and set the forfeiture rate to 0%. The forfeiture rate is an assumption about the number of people who forfeit (don’t exercise) their shares when they leave GitLab. The actual rate is shown in the report and calculated by Carta.
* Click [Generate Report](https://docs.google.com/document/d/1NKcuU9EBe3n8wnZiYBJjjM8nD8RfNresBo7mQT0PNnw/edit?usp=sharing)
* The [report is generated](https://docs.google.com/document/d/1o0fhQG0SX_TZwav_xAimmVxCB91UuqONLq0PJ7IWB6M/edit?usp=sharing) and added to the list
* Click on the button at the right and select [Download US GAAP consolidated grant date report](https://docs.google.com/document/d/1bvb98tycYY2WDqlIXhFkxJHK5ZXzG3qXR8ual6ZKa1A/edit?usp=sharing). This is the only report that is used. The Mark to Market Report isn’t used.
* The report comes out as an Excel file, and is saved to a Google Drive by the Accounting and External Reporting Manager.

**Accounting**

* Only [3 tabs](https://docs.google.com/document/d/1OIoNHHVR-3ZE9VN1v9gVnGiDEY3C8mrt-ySYR2kX_DY/edit?usp=sharing) are used: Expense Summary, Cost Center, and the time period.
* The Cost Center tab summarizes the expense by the department and is booked to NetSuite at a cost center level. 
* The department expenses are shown [here](https://docs.google.com/document/d/1zFy9BoXnLl7rW2G7pWbiH82EDUxmqjFlJP65GDYLClc/edit?usp=sharing)
* If any expense shows up as NA (the department isn’t entered into Carta) then the Sr. Manager, Accounting and External Reporting needs to go in and find out what department that is, and manually adjust the report. The adjustments are done either by the Stock Options Administrator or the Sr. Manager, Accounting and External Reporting.
* The expenses are booked department wise as shown [here](https://docs.google.com/document/d/1-zTCcsDKYcWmpgNXK6BKF8DbGoQl-tx7885H2rLISDw/edit?usp=sharing), with an offset to Additional Paid-in Capital
* Stock option exercises hit cash and other long term liabilities because the majority of exercises done are early exercises (shares haven’t vested). So, if the team member leaves before the shares are done vesting, GitLab buys the shares back, and so a liability associated with this is created. This is tracked with Carta, which calculates the number of shares that move from unvested to vested. So, when the buyback occurs, an entry is booked that takes that amount out of the liability and puts it into equity.
* The [Time Period Tab](https://docs.google.com/document/d/1V3siwQcIJbBaQ4sGw9HzfjNmjYUcy6NvlgyU6f7Nebk/edit?usp=sharing) shows all the calculations, so if any questions arise regarding any differences or workings, then this tab is checked.
* Cash Flow is done using the [Equity Cash file](https://docs.google.com/document/d/1N_aurlpq-pc6ZUIhp2AJGJVM1aPLNIA4ljHHvykWLGs/edit?usp=sharing), which is put together by the Senior Accounting Manager’s team.
* Any line item related to Equity is marked, so that the Sr. Manager, Accounting and External Reporting team can investigate and take the appropriate actions.
* The [Equity Cash file](https://docs.google.com/document/d/1Nu6VMO6GAir_19TYSLVnPNZamN9m1kqtf-ANICefLj4/edit?usp=sharing) also includes the options exercised report (showing the details of all the team members who’ve exercised shares), the options exercise in progress report (showing the details of all team members who’ve started the exercise process, but didn’t get their exercise approved yet), and the canceled report (showing the details of all the team members who’ve left GitLab while owning shares).
* Shares Repurchased show up in the [canceled report](https://docs.google.com/document/d/17CQLdKTq5Y4JB7oMFcr1FQN2ZAyZcqU54ZykvS-Tbfk/edit?usp=sharing). The amount is calculated by multiplying the number of shares repurchased and the price of each share. This should equal the amount of cash shown in the report.
* This calculation is done manually at the side.
* That amount should match the [cash shown in the report](https://docs.google.com/document/d/1u4z_f9csZAtOaNNDb8GzD0Pjp2wx144po6MdMt4WpSw/edit?usp=sharing).
* Similarly, the total exercise value (exercise price * Number of shares) shown in the [options exercised report](https://docs.google.com/document/d/1tcud8nGLr5MzA9b8HuhaUVXzkX_GtZU8HfeBhaLICwk/edit?usp=sharing) should match. This way, the cash received is tied back to the options exercised line by line.
* Any differences are brought up with the Stock Options Administrator and decided upon how to account for.
* The reports are provided to Payroll, and Payroll calculates the taxes.

**Entries in NetSuite: (ESOP.C.05 and ESOP.C.07)**

**Recording of a Long Term Liability at the time of Early Exercise - monthly**

DR bank Account<br>
Cr Other Long term Liability: Cash Liability - Early Stock Option Exercise

Or

DR team member Receivable<br> 
Cr Other Long term Liability: Cash Liability - Early Stock Option Exercise


**Reclass of Vested Options to Equity - monthly**


DR  Other Long term Liability: Cash Liability - Early Stock Option Exercise<br>
Cr Common Stock<br>
Cr APIC - Stock Options - expense accounts

**Recording of Stock Compensation expenses - monthly**

Dr Stock Compensation - COS<br>
Dr Stock Compensation - R&D<br>
Dr Stock Compensation - G&A<br>
Dr Stock Compensation - Marketing<br>
Dr Stock Compensation - Sales<br>
Cr APIC - Stock Options - expense account

**Recording of Repurchases - monthly**

If the repurchase is not completed prior to the team member leaving the company, we are booking a reclass to move the long-term liability to a short-term liability, as we will no longer be settling the long-term liability with equity but instead with cash.

 * Dr Other Long term Liability: Cash Liability - Early Stock Option Exercise<br>
   Cr Other Current Liability

 * Dr Other Current Liability<br>
   Cr Bank Account

# 3.Tax Withholding

[Link to flowchart](https://drive.google.com/open?id=1TJsT9gYWSvTVWsg93S_IFLq9kJW6nNpIcMAQpTUHucQ)

* Taxes levied upon early exercise and taxes withheld vary from country to country and depend on the type of team member as well. For more info on taxes, please visit:
  * [Stock Options Handbook page](/handbook/tax/stock-options/)
  * [Tax Treatment in Australia](/handbook/tax/stock-options/australia/)
  * [Tax Treatment in Belgium](/handbook/tax/stock-options/belgium/)
  * [Tax Treatment in Canada](/handbook/tax/stock-options/canada/)
  * [Tax Treatment in Ireland](/handbook/tax/stock-options/ireland/)
  * [Tax Treatment in Germany](/handbook/tax/stock-options/germany/)
  * [Tax Treatment in Netherlands](/handbook/tax/stock-options/netherlands/)
  * [Tax Treatment in New Zealand](/handbook/tax/stock-options/newzealand/)
  * [Tax Treatment in UK](/handbook/tax/stock-options/unitedkingdom/)
  * [Tax Treatment in USA](/handbook/tax/stock-options/unitedstates/)

* Tax Rates and regulations for the various countries and locations are provided to GitLab by the external Tax consultant that GitLab uses. 

* GitLab reports to the tax authorities only for the stock option exercises of the team members that they directly employ for the period. GitLab does not report the exercises of contractors and PEO team members to the tax authorities. Additionally, GitLab doesn't withhold tax for these team members (contractors and PEO team members) as a part of payroll processing, and subsequently, pay the tax liability for these team members to the tax authorities. For more info please visit [here](/handbook/tax/stock-options/).

* The Stock Options Administrator on a monthly basis pulls the exercise report for the previous month from Carta and uploads it to a [Google Drive folder](https://docs.google.com/document/d/1qjB2xWzUI5e8-LXxnUjeik_n_eRIiEqeYk6ZeJWGtgI/edit?usp=sharing).

* The payroll team takes this data and makes a [copy](https://docs.google.com/document/d/10fGSxogwoz0GAd3aewRLWod7tmeCphSbUd3Cn5kErk0/edit?usp=sharing). 

* On the copy, it identifies the team member’s location. This is done because depending on the location, the exercise may or may not count as taxable income to them.  After the team members’ locations are identified, the payroll team calculates the team member’s tax liability based on the tax rates and regulations provided to GitLab by the external Tax consultant and reports these amounts to payroll as a part of the payroll computations for the month **(ESOP.C.06)**.

* The payroll specialist EMEA is responsible for calculating the exercise tax liabilities for all Non-US locations. These are included in the payroll computations for the month and are reviewed by the Senior Manager, Global Payroll and Payments as a part of payroll procedures **(ESOP.C.06 and REG.C.17)**.

* After the payroll team gets the Exercise report from the [Stock Options Administrator](https://docs.google.com/document/d/1xCdVibw3esRcuyxIdpmoAuGnlyk8COdCscz9ebsxSdA/edit?usp=sharing) and identifies the team members with taxable locations, the payroll specialist sends each team member an email containing the amount to be withheld relating to the exercise.

* If the amount can be deducted from the team member’s salary, then the team member doesn’t have to send an additional payment to GitLab. If the team member wants to send an additional payment to GitLab, then they would send the money to the bank account associated with the respective legal entity (used during exercise). Payroll usually gives the team member about a week’s time to send over the funds. 

* After GitLab receives the funds, the exercise is [reported](https://docs.google.com/document/d/1Gtg6G-3YFgB5rQW1NZ8BP2YL8oQw4Em_ZoNJcQk4W0g/edit?usp=sharing) to the payroll service provider, either as a part of the current month’s payroll or at the latest, the next one. 

* This is done as a part of payroll processing. 

GitLab remits the funds directly to the appropriate tax authorities.

If the team member doesn’t send the funds over to GitLab, then Legal and the CFO get involved.
