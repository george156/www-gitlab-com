---
layout: handbook-page-toc
title: "SLC.1.03 - Release Notes"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SLC.1.03 - Release Notes

## Control Statement

Release notes are available and communicated to internal and external system users through release posts.

## Context

Major changes to the GitLab product are rolled out in the form of a new product version, or release. As part of this process, release notes are created and posted to the [GitLab Release Blog](https://about.gitlab.com/releases/categories/releases/). This is a public blog that is available to all internal and external users.

## Scope

This control is applicable to ALL GitLab releases - full version releases, patches, critical security releases, etc. so that changes to the GitLab product and it's features are transparently communicated to all [GitLab Team Members](/handbook/communication/top-misused-terms/).

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s):
  * Marketing
  * Product Management

## Guidance

All release posts should follow the [GitLab Release Posts](/handbook/marketing/blog/release-posts/) process documented in the handbook. Typically, release posts are created by a Product Manager and Marketing is responsible for ensuring that the post makes it to the GitLab Release Blog. 

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Release notes control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/1712).

### Policy Reference
*  [GitLab Release Posts](/handbook/marketing/blog/release-posts/)

## Framework Mapping

* SOC2 CC
  * CC2.2
