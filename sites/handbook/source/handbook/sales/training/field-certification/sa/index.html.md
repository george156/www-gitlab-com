---
layout: handbook-page-toc
title: "Field Certification: Solutions Architects"
description: "To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a certification program for Solutions Architects that includes functional, soft skills, and technical training for field team members"
---

## Field Certification Program for Solutions Architects 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a certification program for Solutions Architects that includes functional, soft skills, and technical training for field team members.  

Certification badges will align to the critical customer engagements as well as the [field functional competencies](/handbook/sales/training/field-functional-competencies/) that address the critical knowledge, skills, role-based behaviors, processes, content, and tools to successfully execute each customer interaction.

## Solutions Architects Curriculum 
The below slide shows the holistic learner journey for SAs and provides context for the information included throughout the learning program. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQvjB6E9JlplzwqBHVv2fFGAEGZwqjg4AZQO-p_DqjX7znjZGOC_q2-d2xCbwr2LbfXCmyOvVxcirYb/embed?start=false&loop=false&delayms=3000&slide=id.g94bb3b04a3_0_458" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Delivering Value-Driven Demos Learning Path  
This is the first learning path in development for SAs which will consist of 5 courses. The learning objectives for each course are outlined below.

**Course 1: Gather the Technical Requirements through Discovery** 
* Review Customer Requirements document prepared by SDR and SAL
* Perform high-level technical discovery call with customer 

**Course 2: Prepare for the Customer Demo** 
* Analyze the updated requirements from the customer collected during discovery 
* Prepare a pre-demo questionnaire
* Build the customer demo 
* Conduct a demo dry run 

**Course 3: Deliver the Customer Demo** 
* Demonstrate GitLab features, value, propositions, and workflows 
* Present compelling information in a  relevant, tailored, and logical way 

**Course 4: Update Documentation after the Demo** 
* Review and update customer requirements 

*Note: Commercial SAs will have a light-weight version of these modules*

## Recognition
Upon completing each course, the associate will receive a badge. 

## Feedback 
We want to hear from you! You can follow along with course development by checking out the issues on the [Field Cert Team Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426?&label_name[]=field%20certification). 
