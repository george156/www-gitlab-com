---
layout: markdown_page
title: 'The Forrester Wave™: Continuous Integration Tools 2017'
description: "This page represents how Forrester views our CI tools capabilities in relation to the larger market."
canonical_path: "/analysts/forrester-ci/"
---
## GitLab and The Forrester Wave™: Continuous Integration Tools, Q3 2017

This page represents how Forrester views our CI tools capabilities in relation to the larger market and how we're working with that information to build a better product. It is also intended to provide Forrester with ongoing context for how our product is developing in relation to how they see the market.

![Forrester CI Wave](/images/analysts/ci-thumb.png){: .small}

### Forrester's Key Takeaways on the CI TOOLS Market at time of report publication:

**GitLab, Microsoft, CircleCI, And CloudBees Lead The Pack**
Forrester's research uncovered a market in which GitLab, Microsoft, CircleCI, and CloudBees lead the pack. Codeship, AWS, Travis CI, and JetBrains offer competitive options. Atlassian and IBM lag behind.

**AD&D Leaders Empower Their Teams Through Automation**
Adoption of CI tools is growing as more AD&D organizations shift to Agile and DevOps, resulting in smaller, product-centric, autonomous software development teams. AD&D leaders empower their teams with CI tools that automate software delivery tasks and easily integrate with other parts of the tool chain, without unwanted complication and management overhead.

**Pricing Model, Ease Of Use, And Analytical Capability Separate The Vendors**
As AD&D organizations become more distributed, more global, and more reliant on open source components, traditional CI tools no longer suffice. Vendors are taking different approaches to address this evolution. Those with a vision and road map that align best with modern app dev principles will lead the pack.

### Forrester's Take on GitLab
GitLab delivers ease of use, scalability, integration, and innovation. GitLab has a broad market reach, with over 80,000 active instances using the open source GitLab Community edition and over 500 enterprises paying for GitLab CI/CD. GitLab supports development teams with a well-documented installation and configuration processes, an easy-to-follow UI, and a flexible per-seat pricing model that supports self-service. GitLab’s vision is to serve enterprise-scale, integrated software development teams that want to spend more time writing code and less time maintaining their tool chain. The company actively engages in the DevOps community, hosting regular webcasts and sponsoring developer events. It also stands apart from most other vendors in the space by making its planned enhancements road map available to the community through a public issue tracker.

Despite best-in-class extensibility, reference customers cited limitations in notifications and alerts, calling out difficulty integrating with Slack, among other platforms. GitLab will appeal to organizations that want to operate as integrated teams by offering an innovative, end-to-end solution that works for both small and enterprise-scale teams.

### Lessons Learned and Future Improvements

Forrester helped us understand a few areas where we need to improve. See our [thoughts
and upcoming work in each area](https://gitlab.com/groups/gitlab-org/-/epics/509).
