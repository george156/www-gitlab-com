---
layout: job_family_page
title: "Senior Director of Investor Relations"
description: "The Senior Director of Investor Relations drives the strategy and long-term market positioning of GitLab in the public financial markets."
---

## Senior Director, Investor Relations

The Senior Director of Investor Relations drives the strategy and long-term market positioning of GitLab in the public financial markets. The IR leader plays a key role in preparing GitLab to become a multi-billion dollar public company. This position reports to the CFO and works closely with senior leadership across the company including interaction with the executive team. This position will be responsible for developing and executing an effective investor communications strategy, performing financial analysis, and maintaining strong relationships with investors, investment banks and analysts. This role requires a deep understanding of how the IPO process works and will help guide the company on its journey to becoming a publicly traded company. The Director of IR is expected to be deeply immersed in GitLab’s values of transparency and efficiency and has a passionate point of view of how to incorporate those values into GitLab’s investor relations strategy and process.

### Job Grade 

The Senior Director, Investor Relations is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).\

### Responsibilities

1. Develop long term relationships with industry analysts and investors to communicate the company’s financial results and strategic positioning.
1. Design the quarterly earnings process and Q&A topics in partnership with CEO, CFO and executive team.
1. Execute on the Company’s investor education strategy to ensure the GitLab story is well positioned to the top 30 software IPO investors.
1. Play a lead role in drafting the Company’s S1 registration statement.
1. Serve as a key advisor to the CFO on issues involving analyst and investor perception of the long term growth and health of the business.
1. Serve as an on-the-record spokesperson with industry analysts, to advance corporate brand narratives with financial data & insights.
1. Monitor industry developments and market trends, develop executive summaries for senior leadership and the Board of Directors on peer company news, financial and market share performance.
1. Maintain strong relationships with investors by coordinating efficent and effective education events; identify speaking opportunities for e-goup members to position the company favorably at important events.
1. Manage information necessary for preparation of strategic financial and investor communications
1. Prepare quarterly earnings release materials including press releases, conference call scripts, Q&A and slide presentation; prepare other financial press releases and supplemental materials.
1. Liaise with Analyst and Media Relations and Corporate Communications on materials such as press and earnings releases, for dissemination to the public.

### Requirements

1. Seven to ten years relevant experience in Investor Relations and related roles.
1. Excellent writing skills for crafting scripts, financial presentations, investor kits, etc.
1. Confident and strong presentation skills in financial environments and the ability to communicate with sophisticated financial investors.
1. Strong quantitative and qualitative analytical abilities with attention to detail.
1. Established relationships with relevant sell side and buy side analysts a plus.
1. Experience communicating the growth and metrics of subscription-based models a plus.
1. Ability to use GitLab

## Performance Indicators
1. [New Hire Location Factor](/handbook/hiring/metrics/#new-hire-location-factor)
1. [Enterprise Value to Sales](/handbook/finance/investor-relations/#enterprise-value-to-sales)
1. Percent of analysts who describe the company the same way we do
1. Percent of unanticipated questions during earning calls; Target > 80% of questions are anticipated
1. Investor satisfaction
1. Volitality benchmark

## Career Ladder

The next step in the Investor Relations job family is to move to a VP level which is not yet defined at GitLab. 

